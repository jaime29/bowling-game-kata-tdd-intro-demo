# Welcome to **T**est **D**riven **D**evelopment (a limited introduction)

## TDD is about removing human error from software development, now and in the future.

---

### 3 Rules of TDD

1. Only write production code to pass a failing test.
1. Write the minimal test that fails.
1. Write only enough code to pass the test.

_(Note: A test is failing whenever it doesn't **pass**.)_

### TDD Cycle

1. Red - Write the test.
1. Green - Write the code.
1. Refactor - Make it maintainable.

(See: http://www.butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata)
